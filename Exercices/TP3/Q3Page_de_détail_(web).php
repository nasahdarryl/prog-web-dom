<?php

require_once "../../Helpers/tp3-helpers.php";

if(isset($_GET["id"])){
    construitPage(getArrayForMovie($_GET["id"]));
}
else{
    echo "veillez saisir un ID";
}

function getArrayForMovie($id){
    $JSON = tmdbget("movie/" . $id);
    $fullArray = json_decode($JSON, true);
    $arrayRet = array();
    $arrayRet["Titre original"] = $fullArray["original_title"];
    $arrayRet["Titre"] = $fullArray["title"];
    $arrayRet["Description"] = $fullArray["overview"];
    $arrayRet["Tags"] = $fullArray["tagline"];
    $arrayRet["Page"] = $fullArray["homepage"];
    return $arrayRet;
}

function construitPage($arrayMovie){
    foreach ($arrayMovie as $key => $value) {
        echo $key . " : " . $value . "<br>";
    }
}

?>

