<?php

include('tp3-helpers.php');

// l'ID est saisie par l'utilisateur sur le formulaire
if (isset($_POST['id_film'])) {
    $id_film = $_POST['id_film'];

    $url_component = "movie/" . $id_film;

    // requetes 3 langues 
    
    $contenu_brut=tmdbget($url_component,null);
    $contenu_json=json_decode($contenu_brut,true);

    $content_or = tmdbget($url_component, ['language' =>   $contenu_json["original_language"]]);
    $content_fr = tmdbget($url_component, ['language' => 'fr']);
    $content_en = tmdbget($url_component, ['language' => 'en']);

    // tableaux des 3 langues
    $content_array_or = json_decode($content_or, true);
    $content_array_fr = json_decode($content_fr, true);
    $content_array_en = json_decode($content_en, true);
 
 //Question 5 image 
 
 
    // traitement image
    $base_url = "https://image.tmdb.org/t/p/";
    $size = "w500";
    $file_path_or = $content_array_or["poster_path"];
    $file_path_fr = $content_array_fr["poster_path"];
    $file_path_en = $content_array_en["poster_path"];

    // src image
    $src_image_or = $base_url . $size . $file_path_or;
    $src_image_fr = $base_url . $size . $file_path_fr;
    $src_image_en = $base_url . $size . $file_path_en;
 
 
    // question 10 : bande annonce 
    
 
    $url_component_video = "movie/" . $id_film . "/videos";
    $content_video = tmdbget($url_component_video, null);
    $content_video_array = json_decode($content_video, true);
    foreach ($content_video_array["results"] as $video) {
        if ($video["type"] == "Trailer") {
            $code_video = $video["key"];
            $link = "https://www.youtube.com/embed/" . $code_video;
            break;
        }
    }
} else {
    echo "Veuillez renseigner un identifiant de film !";
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="style.css" rel="stylesheet">
    <title>Utilisation de TMDB</title>
</head>

<body>
    <form method="POST" action="">
        <input type="text" name="id_film" placeholder="ID du film" />
        <button type="submit">Valider</button>
    </form>

    <h1>Info du film "<?php echo $content_array_fr['title'] . " (" . $content_array_or['title'] . ")" ?>"</h1>
    <table style="width:100%">
        <tr>
            <th class="info">Infos</th>
            <th>Version O</th>
            <th>Version EN</th>
            <th>Version FR</th>
        </tr>
        <tr>
            <td class="info">Titre</td>
            <td><?php echo $content_array_or['title'] ?></td>
            <td><?php echo $content_array_en['title'] ?></td>
            <td><?php echo $content_array_fr['title'] ?></td>
        </tr>
        <tr>
            <td class="info">Titre original</td>
            <td><?php echo $content_array_or['original_title'] ?></td>
            <td><?php echo $content_array_en['original_title'] ?></td>
            <td><?php echo $content_array_fr['original_title'] ?></td>
        </tr>
        <tr>
            <td class="info">Tagline</td>
            <td><?php echo $content_array_or['tagline'] ?></td>
            <td><?php echo $content_array_en['tagline'] ?></td>
            <td><?php echo $content_array_fr['tagline'] ?></td>
        </tr>
        <tr>
            <td class="info">Description</td>
            <td><?php echo $content_array_or['overview'] ?></td>
            <td><?php echo $content_array_en['overview'] ?></td>
            <td><?php echo $content_array_fr['overview'] ?></td>
        </tr>
        <tr>
            <td class="info">Page TMDB</td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=en-US" ?>"> TMDB Link</a></td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=en-US" ?>"> TMDB Link</a></td>
            <td><a href="https://www.themoviedb.org/movie/<?php echo $id_film . "?language=fr-FR" ?>"> TMDB Link</a></td>
        </tr>
        <tr>
            <td class="info">Image</td>
            <td><img src="<?php echo $src_image_or ?>" alt="Poster of the movie"></td>
            <td><img src="<?php echo $src_image_en ?>" alt="Poster of the movie"></td>
            <td><img src="<?php echo $src_image_fr ?>" alt="L'affiche du film"></td>
        </tr>
    </table>

    <!-- Question 10 : bande annonce du film -->
    <p align="center"><iframe width="400" height="300" src="<?php echo $link ?>"></p>
    </iframe>

</body>

</html>
