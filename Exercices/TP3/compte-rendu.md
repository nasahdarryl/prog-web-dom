% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : TP3 TMDB

## Participants 

* BENHAMED ABDELHAKIM
* Darryl Jordan 
 
## Mise en jambes utilisation de The Movie Database
 
## Question 1 Exploration
 
 -Le format de réponse est en JSON.
 -Le film est Fight Club On peut le savoir avec du champs original_title.
 -En rajoutant le paramètre supplémentaire language=fr comme si-dessous : 

 http://api.themoviedb.org/3/movie/550?api_key=ebb02613ce5a2ae58fde00f4db95a9c1&language=fr 
 
 la page s'affiche en français avec les memes informations. 
 
## Question 2 Exploration CLI 

 -En appelant l'API avec curl, nous obtenant la même réponse que la question précédente.
 -Nous avons crée un programme php minimal "Q2_exploration_cli.php" qui inclu en utilisant require en fichier header "tp3_helpers.php" et qui utilise les fonctions de ce dernier. 
On utilise d'abord la fonction tmdbget() pour obtenir la réponse de l'api, puis nous décodons grâce à json_decode() la réponse pour la stocker dans un tableau. 
la première fonction attend comme paramètre le chemin relatif du film soit "movie/550" et comme second rien car nous n'avons pas besoin de préciser des paramètres. 
Enfin, on affiche le tableau. 

## Question 3 Page de détail (web)  
 

On a crée deux fonctions :
 getArrayMovie($id) et construitPage($arrayMovie) 
 -getArrayMovie($id)  renvoie un tableau PHP pour un film dont on passe l’id en paramètre. 
 On utilise la fonction tmdbget() pour récupérer le json sous forme de string, ensuite on utilise la fonction json_decode avec comme second paramètre true pour récupérer les information du json sous forme de tableau PHP. 
 Ensuite on forme un nouveau tableau sous la forme “clé” => “valeur” qu’on retourne. 
 
 -construitPage($arrayMovie) fait un foreach sur le tableau créé précédemment en affichant la clé puis la valeur.

Le point fort de cette conception est que pour ajouter l’affichage d’un champ en plus il suffit de rajouter une ligne dans la fonction getArrayMovie(). 

# Mise en jambes analyse d’un flux RSS de podcast  


# Les choses sérieuses utilisation de The Movie Database  
 
# Question 4 

Pour avoir les informations sur le film fourni par son identifiant en trois langues dans un tableau  : originale, française et anglaise, On a appelé trois fois l'API avec comme paramètre la langue. 
pour la langue originale on a recuperé sa langue  originale de l'api avec un parametre null qu'on a decodé pour recuperer la langue et refait un appel avec comme parametre la langue originale.
Ensuite nous décodons tout cela et nous pouvons afficher les informations dans la partie HTML.

# Question 5 

pour l'affiche, on avait besoin de trouver l'adresse de l'image, elle est composé d'une base url, de la taille et du chemin qui se trouve dans la réponse de l'API. 
Enfin cette adresse sera renseigné dans l'attribut "src" de la balise "img". 
on a ajouté egalement un attribue alt pour afficher un texte alternatif si l'image ne s'affiche pas. 
# Question 6   
  
 Nous voulons, cette fois-ci afficher la collection de nom en relation avec le film que l'utilisateur rentrera dans le champ vide.
 pour cela on a fait l'appel a l'API comme on l'a fait précedamment  mais en changant l'url du chemin relatif (premier paramètre de tmdbget()) par "search/collection". 
  On a mit le résultat dans un tableau a l'aide de json_decode() et vérifier s'il y a un résultat. 
   Ensuite nous pouvons afficher l'ensemble des films car nous avons l'id de la collection. Et donc il nous faut appelé encore une fois tmdget() avec comme chemin relatif "collection/id_collection" où id_collection se trouve dans le premier tableau. On obtient ensuite l'ensemble des films avec quelques informations notamment l'id de chaque film, le titre et la date de sortie. On affiche ces derniers sur la page.

# Question 7 
  
 On affiche tous les acteurs en ajoutant au chemin relatif "/credits", nous avons décidé de créer un tableau qui stocke tous les acteurs en faisant attention qu'il n'y ait pas de doublons. On vérifie donc cela grâce à la fonction in_array(). Puis nous affichons sur la page la liste des acteurs en appelant le tableau.


# Question 8    

Il est possible d'afficher tous les acteurs qui jouent des hobbits. On regarde dans le tableau de la question précédente contenant tous les acteurs ainsi que leurs "characters", si ce champ comporte "Hobbit", "Frodo", "Bilbo", "Pippin", "Merry", alors ce sont des acteurs qui jouent des hobbits et on stocke leurs noms dans un tableau nommé "liste_acteur_hobbit".

# Question 9

Le lien-rebond appellera le fichier "traitement_q9". Nous passons en paramètre de ce lien l'id de l'acteur que l'on récupère dans ce fichier afin d'appeler l'API avec cet ID et enfin, obtenir tous les films dans lequel il a joué. On change le chemin relatif par : "person/id_acteur/movie_credits".
# Question 10   
 
Dans le fichier "Question-3-4-10.php" 
 on appelle l'API avec le chemin relatif "movie/id_film/videos", ensuite nous décodons la réponse. 
 Dans le tableau généré avec json_decode() on trouve un champ "type" qui nous permet de savoir si c'est un trailer ou non, on effectue une condition pour vérifier si c'est un trailer, si oui on crée une variable "link" qui est composé du lien youtube et de la clé de la video que l'on trouve dans le champ "key" du tableau. 
  Enfin avec ce lien, nous pouvons, dans la partie HTML, mettre le lien dans l'attribut "src" de la balise "iframe".

