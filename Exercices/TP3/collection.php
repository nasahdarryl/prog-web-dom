<?php
include('tp3-helpers.php');

 
if (isset($_POST['collection_name'])) {

$collection_name = $_POST['collection_name'];}else{ 
// Sécurité pour la première connexion à la page il affiche directement la collection seigneur des anneaux
    $collection_name = "Le seigneur des anneaux";
} 
  // Sécurité si l'utilisateur laisse le champs vide
  if ($collection_name==""){
    $collection_name = "Le seigneur des anneaux";
  }

 
$url_component = "/search/collection";

 // on accede a l'api de collection avec comme parametre query
    $content = tmdbget($url_component, ['query' => $collection_name]);
    $content_array = json_decode($content, true);

    if ($content_array['total_results'] === 0) {
        echo "Cette collection est introuvable";
        echo "<br/>";
    }

    $movies = array();
    foreach ($content_array['results'] as $collection) {

        // on accede a l'api avec id du film que l'on a trouvé dans l'api de collection
        $url = "collection/" . $collection['id'];
        $content_movie = tmdbget($url, null);
        $content_movie_array = json_decode($content_movie, true);

        $credits = array();

        // on met dans le tableau movie des infos sur les films
        foreach ($content_movie_array['parts'] as $info) {

            $movies[] = array(
                'id' => $info['id'],
                'title' => $info['title'],
                'release_date' => $info['release_date']
            );

            // liste des acteurs (credits)
            $url2 = "movie/" . $info['id'] . "/credits";
            $content_credit = tmdbget($url2, null);
            $content_credit_array = json_decode($content_credit)->{'cast'};;
            foreach ($content_credit_array as $actor) {
                $name = $actor->{'name'};
                $character = $actor->{'character'};
                $id = $actor->{'id'};
                if (!in_array($name, $credits)) {
                    $credits[$name] = array($name, $character, $id);
                }
            }
        }
    }
    // liste acteur qui joue Hobbit
    $liste_acteur_hobbit = array();
    foreach ($credits as $hobbit) {
        if (($hobbit[1] == "Hobbit (uncredited)") || ($hobbit[1] == "Party Hobbit (uncredited)") || ($hobbit[1] == "Cute Hobbit Child (uncredited)") || ($hobbit[1] == "Village Female Hobbit (uncredited)") || ($hobbit[1] == "Hobbit Band Member (uncredited)") || ($hobbit[1] == "Orc / Goblin / Wildman / Hobbit (uncredited)") || ($hobbit[1] == "Frodo") || ($hobbit[1] == "Bilbo") || ($hobbit[1] == "Pippin") || ($hobbit[1] == "Merry")) {
            $liste_acteur_hobbit[] = array("name" => $hobbit[0]);
        }
    }
    //print_r($credits);
    //print_r($liste_acteur_hobbit);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="style.css" rel="stylesheet">
    <title>Collection</title>
</head>

<body>
    <form method="POST" action="">
        <input type="text" name="collection_name" placeholder="Nom de la collection" />
        <button type="submit">Valider</button>
    </form>

    <div>

        <h1>Collection</h1>
        <?php foreach ($movies as $data) { ?>
            Identifiant : <?php echo $data['id'];
                            echo "<br/>"; ?>
            Titre : <?php echo $data['title'];
                    echo "<br/>"; ?>
            Date de sortie : <?php echo $data['release_date'];
                                echo "<br/>"; ?>
        <?php } ?>
 
 
    <h1>Liste des acteurs</h1>
        <?php foreach ($credits as $name) { ?>
            <?php $actor_id = $name[2];
            $link = "traitement_q9.php?actor_id=" . $actor_id;
            ?>
            <?php echo $name[0] . " --> " . $name[1] . "<a href='" . $link . "'> lien </a>" ?>
            <?php echo '<br/>'; ?>
        <?php } ?>

        <h1>Liste des acteurs qui jouent le Hobbit</h1>
        <?php foreach ($liste_acteur_hobbit as $hob) { ?>
            <?php echo $hob["name"] ?>
            <?php echo '<br/>'; ?>
        <?php } ?>


    </div>

</body>

</html>

